# Operating System packer templates
## Requirements
Windows packer templates utilize third-party `[windows-update](https://github.com/rgl/packer-provisioner-windows-update)` packer provisioner for updates. Installation and
configuration instruction can be found in the linked github repo.

## Notes
There is a [bug](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=955041) in the virtualbox generating wrong sha hash values.
Deleting hash from Virtualbox .mf files, circumvents this.

Due to [the bug](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1820063) in Ubuntu 20.04 when installing `linux-cloud-tools-common`,
Ubuntu VM can not communicate its IP address to Hyper-V after finishing
autoinstall, and upon boot. Though most articles on internet propose solution
for DHCP with mac binding, this has not proved to be reliable, and this
installation finishes with `Waiting for SSH to become available...`, until timeouts.
Taking all this into account, I have not used DHCP. My solution utilizes usage
of [static IP addressing](https://netplan.io/examples/), paired with packer `ssh_host` parameter.
